// Decompilation by Maxim "Kailo" Telezhenko, 2018

#include <adminmenu>
#include <sdkhooks>
#include <sdktools>
#include "PW/file1.sp"
#include "PW/file2.sp"
#include "PW/file3.sp"
#include "PW/file4.sp"
#include "PW/file5.sp"
#include "PW/file6.sp"
#include "PW/file7.sp"

public Plugin:myinfo = {
	name = "Personal Weapons",
	description = "Персональное оружие игроков + бонусы и инвентарь.",
	author = "DEN (skype: cssrs2_ky39i)",
	url = "www.infozona-51.ru",
	version = "4.3"
}; // address: 37376 // codestart: 0

public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
	CreateNative("GetIDClient_SkinsWeapons", Native_GetIDClient);
	CreateNative("AddWeapon_SkinsWeapons", Native_SetWeapon);
	CreateNative("DelWeapon_SkinsWeapons", Native_DelWeapon);
	CreateNative("CheckSkinWeapon_SkinsWeapons", Native_CheckSkinWeapon);
	CreateNative("CheckWeapon_SkinsWeapons", Native_CheckWeapon);
	CreateNative("AddTime_SkinsWeapons", Native_AddTimeWeapon);
	CreateNative("EnableWeapon_SkinsWeapons", Native_EnableWeapon);
	CreateNative("NewClient_SkinsWeapons", Native_CheckNewClient);
	CreateNative("SetOnSequences_SkinsWeapons", Native_SequenceClient);
	MarkNativeAsOptional("GetUserMessageType");
	MarkNativeAsOptional("PbSetInt");
	MarkNativeAsOptional("PbSetBool");
	MarkNativeAsOptional("PbSetString");
	MarkNativeAsOptional("PbAddString");
	RegPluginLibrary("skins_weapons");

	return APLRes_Success;
}

public OnPluginStart() // address: 1742260
{
	LoadOffsets();
	ConnectDB();
	InitHandles();
	InitAdminMenu();
	HookEvents();
	AutoExecConfig(true, "Skins-Weapons", "sourcemod/skins_weapons");
}

public OnPluginEnd() // address: 1744116
{
	for (new i = 1; i <= MaxClients; ++i)
	{
		for (new j = 0; j < 2; j++)
		{
			if (g_var9616[i][j] > 0 && IsValidEdict(g_var9616[i][j]))
			{
				AcceptEntityInput(g_var9616[infozona][j], "kill");
			}
		}
		if (g_var7504[i] && IsClientInGame(i))
		{
			AddEntityEffects(m_hViewModelPred[i][1], 32); // Array
			TakeEntityEffects(m_hViewModelPred[i][0], 32);
		}
	}
}

public OnMapStart() // address: 1745420
{
	LoadModelsCfg();
}

public OnMapEnd() // address: 1752716
{
	if (g_iRemoveLastConnect > 0)
	{
		CheckTimeClients();
	}
	RemoveExpiredModels();

	ClearCfg();
}

public OnClientDisconnect_Post(client) // address: 1754136
{
	ResetClient(client);
}

public OnClientAuthorized(client, const String:auth[]) // address: 1755352
{
	EventOnClientAuthorized(client, auth);
}

public OnClientPutInServer(client) // address: 1757332
{
	HookClientEvents(client);
}

public OnClientDisconnect(client) // address: 1758864
{
	Func1534408(client);
	ClearClient(client);
}

public OnClientPostAdminCheck(client) // address: 1759784
{
	if (IsClientConnected(client))
	{
		QueryClientConVar(client, "cl_allowdownload", CvarChecking_AllowDownload, client);
	}
}

public CvarChecking_AllowDownload(QueryCookie:cookie, client, ConVarQueryResult:result, const String:cvarName[], const String:cvarValue[]) // address: 1761312
{
	if (StringToInt(cvarValue, 10) == 0)
	{
		KickClient(client, "Включите скачку файлов с сервера: cl_allowdownload 1");
	}
	else
	{
		if (IsClientConnected(client))
		{
			QueryClientConVar(client, "cl_downloadfilter", CvarChecking_DownloadFilter, client);
		}
	}
}

public CvarChecking_DownloadFilter(QueryCookie:cookie, client, ConVarQueryResult:result, const String:cvarName[], const String:cvarValue[]) // address: 1762556
{
	if (strcmp(cvarValue, "all", false))
	{
		KickClient(client, "Включите скачку файлов с сервера: cl_downloadfilter all");
	}
}
