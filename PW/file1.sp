// Decompilation by Maxim "Kailo" Telezhenko, 2018

new m_hOwnerEntity; // address: 1240// zero // codestart: 0
new m_hActiveWeapon, 
	m_iAddonBits; // address: 1244 // zero // codestart: 0
new m_nSequence,
	m_fEffects,
	m_flPlaybackRate,
	m_nViewModelIndex,
	m_hOwner,
	m_hWeapon; // address: 1252 // zero // codestart: 0
new m_PredictableID; // address: 1280 // zero // codestart: 0

LoadOffsets() // address: 1469220
{
	m_hActiveWeapon = FindSendPropOffs("CCSPlayer", "m_hActiveWeapon"); // Array
	m_iAddonBits = FindSendPropOffs("CCSPlayer", "m_iAddonBits"); // controls which items to show on a player
	m_hOwnerEntity = FindSendPropOffs("CBaseCSGrenadeProjectile", "m_hOwnerEntity"); // Array
	m_PredictableID = FindSendPropOffs("CBaseCombatWeapon", "m_PredictableID"); // Array
	m_nSequence = FindSendPropOffs("CPredictedViewModel", "m_nSequence"); // Array
	m_fEffects = FindSendPropOffs("CPredictedViewModel", "m_fEffects"); // Array
	m_flPlaybackRate = FindSendPropOffs("CPredictedViewModel", "m_flPlaybackRate"); // Array
	m_nModelIndex = FindSendPropOffs("CPredictedViewModel", "m_nModelIndex"); // Array
	m_nViewModelIndex = FindSendPropOffs("CPredictedViewModel", "m_nViewModelIndex"); // Array
	m_hOwner = FindSendPropOffs("CPredictedViewModel", "m_hOwner"); // Array
	m_hWeapon = FindSendPropOffs("CPredictedViewModel", "m_hWeapon"); // Array
}

GetAddonBits(iClient) // address: 1471064
{
	return GetEntData(iClient, m_iAddonBits, 4); // Array
}

SetAddonBits(iClient, iBits) // address: 1472396
{
	SetEntData(iClient, m_iAddonBits, iBits, 4, true); // Array
}

GetActiveWeapon(iClient) // address: 1473928
{
	return GetEntDataEnt2(iClient, m_hActiveWeapon); // Array
}

GetOwnerEntity(iEntity) // address: 1475280
{
	return GetEntDataEnt2(iEntity, m_hOwnerEntity); // Array
}

GetSequence(iClient) // address: 1476524
{
	return GetEntData(iClient, m_nSequence, 4); // Array
}

SetSequence(iClient, iSequence) // address: 1478124
{
	SetEntData(iClient, m_nSequence, iSequence, 4, true); // Array
}

Float:GetCycle(iEntity) // address: 1479052
{
	new m_flCycle = FindDataMapOffs(iEntity, "m_flCycle");
	if (m_flCycle != -1)
	{
		return GetEntDataFloat(iEntity, m_flCycle);
	}
	return -1.0;
}

GetEffects(iEntity) // address: 1479924
{
	return GetEntData(iEntity, m_fEffects, 4); // Array
}

SetEffects(iEntity, iEffects) // address: 1481460
{
	SetEntData(iEntity, m_fEffects, iEffects, 4, true); // Array
}

AddEntityEffects(iEntity, iEffects) // address: 1482604
{
	new iCurrentEffects = GetEffects(iEntity);
	if (iCurrentEffects & iEffects)
	{
		return;
	}
	iCurrentEffects = iCurrentEffects | iEffects;
	SetEffects(iEntity, iCurrentEffects);
}

TakeEntityEffects(iEntity, iEffects) // address: 1484924
{
	new iCurrentEffects = GetEffects(iEntity);
	if (!(iCurrentEffects & arg16))
	{
		return;
	}
	iCurrentEffects = iCurrentEffects & ~iEffects;
	SetEffects(iEntity, iCurrentEffects);
}

Float:GetPlaybackRate(iEntity) // address: 1486252
{
	return GetEntDataFloat(iEntity, m_flPlaybackRate); // Array
}

SetPlaybackRate(iEntity, Float:flPlaybackRate) // address: 1488020
{
	SetEntDataFloat(iEntity, m_flPlaybackRate, flPlaybackRate, true); // Array
}

SetModelIndex(iEntity, iModelIndex) // address: 1488988
{
	SetEntData(iEntity, m_nModelIndex, iModelIndex, 4, true); // Array
}

GetViewModelIndex(arg12) // address: 1490860
{
	return GetEntData(arg12, m_nViewModelIndex, 4); // Array
}

GetOwner(iEntity) // address: 1492128
{
	return GetEntDataEnt2(iEntity, m_hOwner); // Array
}

SetOwner(iEntity, iClient) // address: 1493752
{
	SetEntDataEnt2(iEntity, m_hOwner, iClient, true); // Array
}

GetPredictableID(iPredictableID) // address: 1495348
{
	return GetEntData(arg12, iPredictableID, 4); // Array
}

SetPredictableID(arg12, iPredictableID) // address: 1496576
{
	SetEntData(arg12, m_PredictableID, iPredictableID, 4, true); // Array
}
