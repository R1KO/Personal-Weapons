// Decompilation by Maxim "Kailo" Telezhenko, 2018

new g_iClientViewModel[MAXPLAYERS+1]; // address: 11396 // zero // codestart: 0 // end address: 11660

public OnPostThinkPost(client) // address: 1536368
{
	Func1522832(client);
	new iWeapon = GetActiveWeapon(client);
	if (g_var8560[client] && g_var8560[client] < GetTime() && iWeapon > 0)
	{
		Func1539832(client, iWeapon);
		return;
	}
	if (!IsValidEdict(m_hViewModelPred[client][0]))
	{
		return;
	}
	new iSequence = GetSequence(m_hViewModelPred[client][0]);
	if (iWeapon < 1)
	{
		if (g_var7504[client])
		{
			AddEntityEffects(m_hViewModelPred[client][1], 32); // Array
			TakeEntityEffects(m_hViewModelPred[client][0], 32);
			g_var7504[client] = 0;
			g_iClientSequence[client] = 0;
			g_fClientSequenceTime[client] = 0.0;
		}
		g_iClientWeapon[client] = iWeapon;
		return;
	}
	new Float:fGameTime = GetGameTime();
	new Float:fCycle = GetCycle(m_hViewModelPred[client][0]);
	if (iWeapon != g_iClientWeapon[client] && !Func1541204(client, iWeapon, iSequence))
	{
		g_iClientWeapon[client] = iWeapon;
		return;
	}
	if (g_var7504[client])
	{
		if (IsValidEdict(m_hViewModelPred[client][1])) // Array
		{
			SetPlaybackRate(m_hViewModelPred[client][1], GetPlaybackRate(m_hViewModelPred[client][0])); // Array
			if (g_var9352[client])
			{
				if (fCycle < g_fClientCycle[client] && iSequence == g_iClientSequence[client])
				{
					SetSequence(m_hViewModelPred[client][1], 0); // Array
					g_fClientSequenceTime[client] = 0.02 + fGameTime;
				}
				else
				{
					if (g_fClientSequenceTime[client] < fGameTime)
					{
						SetSequence(m_hViewModelPred[client][1], iSequence); // Array
					}
				}
			}
			else
			{
				if (iSequence != g_iClientSequence[client])
				{
					g_var9352[client] = 1;
				}
			}
		}
	}
	if (g_var7240[client])
	{
		g_var7240[client] = 0;
		if (g_var7504[client])
		{
			AddEntityEffects(m_hViewModelPred[client][0], 32);
		}
	}
	g_iClientWeapon[client] = iWeapon;
	g_iClientSequence[client] = iSequence;
	g_fClientCycle[client] = fCycle;
}

Func1539832(iClient, iWeapon) // address: 1539832
{
	decl String:szWeapon[32];
	decl String:var84[50];
	GetEdictClassname(iWeapon, szWeapon, sizeof(szWeapon));
	new var88 = GetStartWeaponName(szWeapon);
	if (Func1651704(iClient, szWeapon[var88], var84))
	{
		Func1632396(iClient, szWeapon[var88], var84, 1);
		Func1541204(iClient, iWeapon, GetSequence(m_hViewModelPred[iClient][0]));
		PrintToChat(iClient, "[Skins Weapons] < %s > удален по истечению времени!", var84);
	}
	g_var8560[iClient] = 0;
}

Func1541204(iClient, iWeapon, arg20) // address: 1541204
{
	g_fClientSequenceTime[iClient] = 0.0;
	new var4 = 0;
	if (!var4)
	{
		new var8 = 0;
		if (g_var7768[iClient])
		{
			decl String:szWeapon[32];
			GetEdictClassname(iWeapon, szWeapon, sizeof(szWeapon));
			new var44 = GetStartWeaponName(szWeapon);
			new iModels[4];
			GetClientWeaponModels(iClient, iModels, szWeapon[var44]);
			if (iModels[1]) // Array
			{
				g_var8560[iClient] = iModels[3]; // Array
				if (IsValidEdict(m_hViewModelPred[iClient][1])) // Array
				{
					AddEntityEffects(m_hViewModelPred[iClient][0], 32);
					TakeEntityEffects(m_hViewModelPred[iClient][1], 32); // Array
					SetSequence(m_hViewModelPred[iClient][1], arg20); // Array
					SetModelIndex(m_hViewModelPred[iClient][1], iModels[1]); // Array; Array
					g_iClientViewModel[iClient] = iModels[1]; // Array
					SetSequence(m_hViewModelPred[iClient][1], arg20); // Array
					if (iModels[2] && strcmp(szWeapon[var44], "c4", true)) // Array
					{
						new var64 = GetPlayerWeaponSlot(iClient, 2);
						if (var64 != -1)
						{
							SetOwner(m_hViewModelPred[iClient][1], var64); // Array
						}
					}
					else
					{
						SetOwner(m_hViewModelPred[iClient][1], iWeapon); // Array
					}
					SetSequence(m_hViewModelPred[iClient][1], arg20); // Array
					SetPlaybackRate(m_hViewModelPred[iClient][1], GetPlaybackRate(m_hViewModelPred[iClient][0])); // Array
					g_var7504[iClient] = 1;
					var4 = 1;
				}
			}
			else
			{
				g_var8560[iClient] = 0;
			}
			var8 = iModels[0]; // Array
		}
		if (!var4 && g_var7504[iClient])
		{
			TakeEntityEffects(m_hViewModelPred[iClient][0], 32);
			if (IsValidEdict(m_hViewModelPred[iClient][1])) // Array
			{
				AddEntityEffects(m_hViewModelPred[iClient][1], 32); // Array
				SetSequence(m_hViewModelPred[iClient][1], 0); // Array
			}
			g_var7504[iClient] = 0;
			g_fClientSequenceTime[iClient] = 0.0;
		}
		if (var8 > 0)
		{
			SetEntProp(iWeapon, Prop_Send, "m_iWorldModelIndex", var8, 4, 0);
		}
		SetPredictableID(iWeapon, var8);
	}
	return var4;
}

public OnWeaponEquipPost(client, weapon) // address: 1544836
{
	if (g_var7768[client])
	{
		decl String:szWeapon[32];
		GetEdictClassname(weapon, szWeapon, sizeof(szWeapon));
		new iStartIndex = GetStartWeaponName(szWeapon);
		new var40 = GetClientWeaponWorldModel(client, szWeapon[iStartIndex]);
		new iPredictableID = GetPredictableID(weapon);
		if (g_iCvarRestrictMode > 0 && iPredictableID > 0 && iPredictableID != var40)
		{
			Func1650448(client, iPredictableID, szWeapon[iStartIndex]);
			SetEntProp(weapon, Prop_Send, "m_iWorldModelIndex", iPredictableID, 4, 0);
			SetPredictableID(weapon, iPredictableID);
		}
		else
		{
			if (var40 > 0)
			{
				SetEntProp(weapon, Prop_Send, "m_iWorldModelIndex", var40, 4, 0);
				SetPredictableID(weapon, var40);
			}
		}
	}
}

GetStartWeaponName(const String:szWeapon[]) // address: 1546656
{
	return (StrContains(szWeapon, "weapon_", false) == 0) ? 7:1;
}
