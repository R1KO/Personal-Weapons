// Decompilation by Maxim "Kailo" Telezhenko, 2018

new g_iCvarRestrictMode = 0; // address: 1676 // codestart: 0
enum ClientID
{
	String:szClientID[16],
	iClientID
};
new g_eiClientID[MAXPLAYERS+1][ClientID]; // address: 1676 // zero // codestart: 0
new m_hViewModelPred[MAXPLAYERS+1][2]; // address: 6432 // zero // codestart: 0
new modelprecache = 0; // address: 7224 // codestart: 0
new g_iObserverMode = 0; // address: 7228 // codestart: 0
new m_hMyWeapons = 0; // address: 7232 // codestart: 0
new g_var7236 = 0; // address: 7236 // codestart: 0
new g_var7240[MAXPLAYERS+1]; // address: 7240 // zero // codestart: 0
new g_var7504[MAXPLAYERS+1]; // address: 7504 // zero // codestart: 0
new g_var7768[MAXPLAYERS+1]; // address: 7768 // zero // codestart: 0
new g_iClientSequence[MAXPLAYERS+1]; // address: 8032 // zero // codestart: 0
new g_iClientWeapon[MAXPLAYERS+1]; // address: 8296 // zero // codestart: 0
new g_var8560[MAXPLAYERS+1]; // address: 8560 // zero // codestart: 0
new Float:g_fClientCycle[MAXPLAYERS+1]; // address: 8824 // zero // codestart: 0
new Float:g_fClientSequenceTime[MAXPLAYERS+1]; // address: 9088 // zero // codestart: 0
new g_var9352[MAXPLAYERS+1] = {1, ...}; // address: 9352 // codestart: 0
new g_var9616[MAXPLAYERS+1][2]; // address: 9616 // zero // codestart: 0
new g_var10408[MAXPLAYERS+1]; // address: 9616 // zero // codestart: 0
// 10672 global address

HookEvents() // address: 1498260
{
	if (GetEngineVersion() == EngineVersion:2)
	{
		HookEvent("player_team", OnPlayerDeath, EventHookMode_Post);
		g_iObserverMode = 3;
	}
	else
	{
		g_iObserverMode = 4;
	}
	m_hMyWeapons = FindSendPropOffs("CBasePlayer", "m_hMyWeapons");
	modelprecache = FindStringTable("modelprecache");
	HookEvent("player_death", OnPlayerDeath, EventHookMode_Post);
	HookEvent("bomb_planted", OnBombPlanted, EventHookMode_Post);
	HookEvent("player_spawn", OnPlayerSpawn, EventHookMode_Post);
	HookEvent("round_start", OnRoundStart, EventHookMode_PostNoCopy);
	for (new i = 1; i <= MaxClients; ++i)
	{
		if (IsClientConnected(i) && IsClientInGame(i))
		{
			OnClientPutInServer(i);
			m_hViewModelPred[i][0] = GetEntPropEnt(i, Prop_Send, "m_hViewModel", 0);
			new var8 = MaxClients + 1;
			while ((var8 = FindEntityByClassname(var8, "predicted_viewmodel")) != -1)
			{
				if (GetOwner(var8) == i)
				{
					if (GetViewModelIndex(var8) == 1)
					{
						m_hViewModelPred[i][1] = var8; // Array
						break;
					}
				}
			}
		}
	}
}

public OnEntityCreated(entity, const String:classname[]) // address: 1503416
{
	if (StrEqual(classname, "predicted_viewmodel", false))
	{
		SDKHook(entity, SDKHookType:24, OnEntitySpawned);
	}
	else
	{
		if (StrContains(classname, "_projectile", false) != -1)
		{
			SDKHook(entity, SDKHookType:24, OnProjectileSpawned);
		}
		else
		{
			if (StrContains(classname, "weapon_", false) == 0)
			{
				SDKHook(entity, SDKHookType:24, OnWeaponSpawn);
			}
		}
	}
}

public OnEntitySpawned(entity) // address: 1504884
{
	new iOwner = GetOwner(entity);
	if (0 < iOwner <= MaxClients)
	{
		switch (GetViewModelIndex(entity))
		{
			case 0:
			{
				m_hViewModelPred[iOwner][0] = entity;
			}
			case 1:
			{
				m_hViewModelPred[iOwner][1] = entity; // Array
			}
		}
	}
}

public OnProjectileSpawned(entity) // address: 1506620
{
	new iOwner = GetOwnerEntity(entity);
	if (IsValidClientIndex(iOwner) && g_var7768[iOwner])
	{
		new iWeapon = GetActiveWeapon(iOwner);
		if (iWeapon != -1)
		{
			Func1516124(iWeapon, entity);
		}
	}
}

public OnWeaponSpawn(entity) // address: 1507928
{
	SetPredictableID(entity, 0);
}

public OnBombPlanted(Handle:event, const String:name[], bool:dontBroadcast) // address: 1509348
{
	new iClient = GetClientOfUserId(GetEventInt(event, "userid"));
	if (!iClient || !g_var7768[iClient])
	{
		return;
	}
	new iC4 = FindEntityByClassname(MaxClients + 1, "planted_c4");
	if (iC4 != -1)
	{
		new iModelIndex = GetClientC4Model(iClient);
		if (iModelIndex != -1)
		{
			decl String:szModel[256];
			szModel[0] = '\0'; // Array
			ReadStringTable(modelprecache, iModelIndex, szModel, sizeof(szModel));
			if (szModel[0]) // Array
			{
				SetEntityModel(iC4, szModel);
			}
		}
	}
}

public OnPlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast) // address: 1511448
{
	new iClient = GetClientOfUserId(GetEventInt(event, "userid"));
	if (!iClient)
	{
		return;
	}
	if (IsClientInGame(iClient) && IsPlayerAlive(iClient) && !IsClientObserver(iClient))
	{
		g_var7240[iClient] = 1;
	}
}

public OnRoundStart(Handle:event, const String:name[], bool:dontBroadcast) // address: 1512920
{
	for (new i = 1; i <= MaxClients; i++)
	{
		g_var10408[i] = 0;
		for (new j = 0; j < 2; j++)
		{
			g_var9616[i][j] = 0;
		}
	}
}

public OnPlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) // address: 1514464
{
	new iClient = GetClientOfUserId(GetEventInt(event, "userid"));
	if (!iClient || !IsClientInGame(iClient) || IsPlayerAlive(iClient))
	{
		return;
	}
	Func1534408(iClient);
}

Func1516124(arg12, arg16) // address: 1516124
{
	new var4 = GetPredictableID(arg12);
	if (var4 > 0)
	{
		decl String:var260[256];
		var260[0] = '\0'; // Array
		ReadStringTable(modelprecache, var4, var260, 256);
		if (var260[0]) // Array
		{
			SetEntityModel(arg16, var260);
		}
	}
}

public OnWeaponDropPost(client, weapon) // address: 1517484
{
	if (g_var7768[client] && weapon > 0)
	{
		new var4 = GetPredictableID(weapon);
		decl String:var36[32];
		GetEdictClassname(weapon, var36, 32);
		new var40 = GetStartWeaponName(var36);
		new var44 = GetClientWeaponWorldModel(client, var36[var40]);
		if (g_iCvarRestrictMode > 0 && var4 > 0 && var44 != var4)
		{
			Func1655144(client, var36[var40]);
		}
		if (var4 > 0)
		{
			CreateTimer(0.0, Timer_SetWorldModel, EntIndexToEntRef(weapon), 2);
		}
	}
}

public Action:CS_OnCSWeaponDrop(client, weaponIndex) // address: 1519668
{
	if (g_var7768[client])
	{
		new var4 = GetPredictableID(weaponIndex);
		decl String:var36[32];
		GetEdictClassname(weaponIndex, var36, 32);
		new var40 = GetStartWeaponName(var36);
		new var44 = GetClientWeaponWorldModel(client, var36[var40]);
		if (g_iCvarRestrictMode > 0 && var4 > 0 && var44 != var4)
		{
			Func1655144(client, var36[var40]);
		}
		if (var4 > 0)
		{
			CreateTimer(0.0, Timer_SetWorldModel, EntIndexToEntRef(weaponIndex), 2);
		}
	}
}

public Action:Timer_SetWorldModel(Handle:timer, any:data) // address: 1521604
{
	new var4 = EntRefToEntIndex(data);
	if (var4 != -1)
	{
		new var8 = GetPredictableID(var4);
		if (var8 > 0 && GetEntProp(var4, Prop_Data, "m_iState", 4, 0) == 0)
		{
			SetEntProp(var4, Prop_Send, "m_iWorldModelIndex", var8, 4, 0);
		}
	}
	return Plugin_Stop;
}

Func1522832(arg12) // address: 1522832
{
	new var4 = GetAddonBits(arg12);
	new var8 = 0;
	if (var4 & 64)
	{
		if (!(g_var10408[arg12] & 64))
		{
			if (g_var9616[arg12][0] > 0 && IsValidEdict(g_var9616[arg12][0]))
			{
				AcceptEntityInput(g_var9616[arg12][0], "kill", -1, -1, 0);
			}
			g_var9616[arg12][0] = 0;
			new var12 = GetPlayerWeaponSlot(arg12, 0);
			if (var12 != -1)
			{
				Func1526428(arg12, var12, 0, "primary");
			}
		}
	}
	else
	{
		if (g_var10408[arg12] & 64)
		{
			if (g_var9616[arg12][0] > 0 && IsValidEdict(g_var9616[arg12][0]))
			{
				AcceptEntityInput(g_var9616[arg12][0], "kill", -1, -1, 0);
			}
			g_var9616[arg12][0] = 0;
		}
	}
	if (var4 & 16)
	{
		if (!(g_var10408[arg12] & 16))
		{
			if (g_var9616[arg12][1] > 0 && IsValidEdict(g_var9616[arg12][1])) // Array; Array
			{
				AcceptEntityInput(g_var9616[arg12][1], "kill", -1, -1, 0); // Array
			}
			g_var9616[arg12][1] = 0; // Array
			new var12 = GetPlayerWeaponSlot(arg12, 4);
			if (var12 != -1)
			{
				Func1526428(arg12, var12, 1, "c4");
			}
		}
	}
	else
	{
		if (g_var10408[arg12] & 16)
		{
			if (g_var9616[arg12][1] > 0 && IsValidEdict(g_var9616[arg12][1])) // Array; Array
			{
				AcceptEntityInput(g_var9616[arg12][1], "kill", -1, -1, 0); // Array
			}
			g_var9616[arg12][1] = 0; // Array
		}
	}
	if (g_var9616[arg12][0])
	{
		var8 = var8 | 64;
	}
	if (g_var9616[arg12][1]) // Array
	{
		var8 = var8 | 16;
	}
	SetAddonBits(arg12, var4 & ~var8);
	g_var10408[arg12] = var4;
}

Func1526428(arg12, arg16, arg20, const String:arg24[]) // address: 1526428
{
	new var4 = GetPredictableID(arg16);
	if (g_var7768[arg12] && var4 > 0)
	{
		if (var4 > 20000000)
		{
			SetPredictableID(arg16, 0);
			return;
		}
		decl String:var260[256];
		var260[0] = '\0'; // Array
		ReadStringTable(modelprecache, var4, var260, 256);
		if (var260[0]) // Array
		{
			g_var9616[arg12][arg20] = CreateEntityByName("prop_dynamic_override", -1);
			DispatchKeyValue(g_var9616[arg12][arg20], "model", var260);
			DispatchKeyValue(g_var9616[arg12][arg20], "spawnflags", "256");
			DispatchKeyValue(g_var9616[arg12][arg20], "solid", "0");
			DispatchSpawn(g_var9616[arg12][arg20]);
			SetEntPropEnt(g_var9616[arg12][arg20], Prop_Send, "m_hOwnerEntity", arg12, 0);
			SetVariantString("!activator");
			AcceptEntityInput(g_var9616[arg12][arg20], "SetParent", arg12, g_var9616[arg12][arg20], 0);
			SetVariantString(arg24);
			AcceptEntityInput(g_var9616[arg12][arg20], "SetParentAttachment", g_var9616[arg12][arg20], -1, 0);
			SDKHook(g_var9616[arg12][arg20], SDKHookType:6, OnTransmit);
		}
	}
}

public Action:OnTransmit(entity, client) // address: 1528976
{
	for (new i = 0; i < 2; i++)
	{
		if (g_var9616[client][i] == entity)
		{
			if (GetEntProp(client, Prop_Send, "m_iObserverMode", 4, 0))
			{
				return Plugin_Continue;
			}
			return Plugin_Handled;
		}
	}
	new iOwner = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity", 0);
	if (GetEntProp(client, Prop_Send, "m_iObserverMode", 4, 0) == g_iObserverMode && iOwner == GetEntPropEnt(client, Prop_Send, "m_hObserverTarget", 0))
	{
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

HookClientEvents(iClient) // address: 1530316
{
	if (g_var7236 == 20)
	{
		SDKHook(iClient, SDKHook_WeaponDrop, OnWeaponDropPost);
	}
	SDKHook(iClient, SDKHook_ThinkPost, OnPostThinkPost);
	SDKHook(iClient, SDKHook_WeaponEquipPost, OnWeaponEquipPost);
}

ResetClient(iClient) // address: 1532380
{
	for (new var4 = 0; var4 < 2; var4++)
	{
		if (g_var9616[iClient][var4] > 0 && IsValidEdict(g_var9616[iClient][var4]))
		{
			AcceptEntityInput(g_var9616[iClient][var4], "kill");
		}
		g_var9616[iClient][var4] = 0;
	}
	g_var7768[iClient] = 0;
}

Func1534408(iClient) // address: 1534408
{
	if (g_var7768[iClient] && IsClientInGame(aiClientg12))
	{
		if (g_var7236 == 20)
		{
			for (new var4 = 0, var8 = -1; var4 < 188; var4 = var4 + 4)
			{
				var8 = GetEntDataEnt2(iClient, m_hMyWeapons + var4);
				if (var8 || !IsValidEdict(var8) || GetPredictableID(var8) < 1)
				{
					continue;
				}
				CreateTimer(0.0, Timer_SetWorldModel, EntIndexToEntRef(var8), 2);
			}
		}
		if (g_iCvarRestrictMode == 2)
		{
			ClearClientDrop(iClient);
		}
	}
}
