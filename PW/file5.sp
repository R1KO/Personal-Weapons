// Decompilation by Maxim "Kailo" Telezhenko, 2018

new Handle:g_hModelsKeyValues = INVALID_HANDLE; // address: 32576
new Handle:g_var32580 = INVALID_HANDLE; // address: 32580
new Handle:g_hClientKeyValues[MAXPLAYERS+1]; // address: 32584 // zero
new Handle:g_var32848 = INVALID_HANDLE; // address: 32848
new Handle:g_hDropKeyValues = INVALID_HANDLE; // address: 32852
new Handle:g_hTrieWeaponNames = INVALID_HANDLE; // address: 32856
// global address 32860

InitHandles() // address: 1603704
{
	g_var32580 = CreateKeyValues("Group");
	g_var32848 = CreateTrie();
	g_hTrieWeaponNames = CreateTrie();
	HookConVarChange(FindConVar("mp_restartgame"), OnConVarRestart);
}

LoadModelsCfg() // address: 1605664
{
	g_hDropKeyValues = CreateKeyValues("Drop");
	decl String:szPath[256];
	BuildPath(Path_SM, szPath, sizeof(szPath), "data/skins_weapons/models.txt");
	g_hModelsKeyValues = CreateKeyValues("Models");
	if (!FileToKeyValues(g_hModelsKeyValues, szPath))
	{
		SetFailState("File not found %s", szPath);
		return;
	}
	new bool:var260 = false;
	if (KvGotoFirstSubKey(g_hModelsKeyValues, true))
	{
		decl String:szModel[256];
		decl String:szWeaponName[52];
		decl String:szWeapon[32];
		decl String:szKey[8];
		do
		{
			KvGetSectionName(g_hModelsKeyValues, szWeapon, sizeof(szWeapon));
			PrepareWeaponName(szWeapon, szWeapon, sizeof(szWeapon));
			if (KvGotoFirstSubKey(g_hModelsKeyValues, true))
			{
				do
				{
					new iValues[5];
					KvGetSectionName(g_hModelsKeyValues, szWeaponName, sizeof(szWeaponName));
					if (GetTrieArray(g_var32848, szWeaponName, iValues, 5))
					{
						var260 = true;
						Format(szWeaponName, sizeof(szWeaponName), "%s_%d", szWeaponName, GetRandomInt(100, 999));
						KvSetSectionName(g_hModelsKeyValues, szWeaponName);
						LogError("An identical ID: Name Change To %s", szWeaponName);
					}
					szModel[0] = '\0'; // Array
					KvGetString(g_hModelsKeyValues, "world", szModel, sizeof(szModel));
					if (szModel[0]) // Array
					{
						iValues[0] = PrecacheModel(szModel, false); // Array
						IntToString(iValues[0], szKey, sizeof(szKey)); // Array
						SetTrieString(g_hTrieWeaponNames, szKey, szWeaponName, true);
					}
					else
					{
						SetFailState("World Model not found - weapon: %s", szWeaponName);
					}
					szModel[0] = '\0'; // Array
					KvGetString(g_hModelsKeyValues, "view", szModel, sizeof(szModel));
					if (!(szModel[0])) // Array
					{
						KvGetString(g_hModelsKeyValues, "view_t", szModel, sizeof(szModel));
						iValues[2] = PrecacheModel(szModel, false); // Array
						KvGetString(g_hModelsKeyValues, "view_ct", szModel, sizeof(szModel));
						iValues[3] = PrecacheModel(szModel, false); // Array
					}
					else
					{
						iValues[1] = PrecacheModel(szModel, false); // Array
					}
					szModel[0] = '\0'; // Array
					if (!strcmp("c4", szWeapon, true))
					{
						KvGetString(g_hModelsKeyValues, "world_planted", szModel, sizeof(szModel));
						if (szModel[0]) // Array
						{
							iValues[4] = PrecacheModel(szModel, false); // Array
						}
					}
					else
					{
						iValues[4] = KvGetNum(g_hModelsKeyValues, "flip_view", 0); // Array
					}
					SetTrieArray(g_var32848, szWeaponName, iValues, 5, true);
					Func1704704(szWeaponName, szWeapon);
				}
				while (KvGotoNextKey(g_hModelsKeyValues, true));
				KvGoBack(g_hModelsKeyValues);
			}
		}
		while (KvGotoNextKey(g_hModelsKeyValues, true));
	}
	KvRewind(g_hModelsKeyValues);
	if (var260)
	{
		KeyValuesToFile(g_hModelsKeyValues, szPath);
	}
	Func1707620();
	BuildPath(Path_SM, szPath, sizeof(szPath), "data/skins_weapons/download.txt");
	if (!ReadDownloadlist(szPath))
	{
		PrintToServer("%s not found", szPath);
	}
}

ClearCfg() // address: 1609116
{
	if (g_hModelsKeyValues)
	{
		CloseHandle(g_hModelsKeyValues);
	}
	if (g_hDropKeyValues)
	{
		CloseHandle(g_hDropKeyValues);
	}
	ClearTrie(g_var32848);
	ClearTrie(g_hTrieWeaponNames);
}

PrepareClient(iClient) // address: 1610108
{
	if (g_hClientKeyValues[iClient])
	{
		CloseHandle(g_hClientKeyValues[iClient]);
		g_hClientKeyValues[iClient] = INVALID_HANDLE;
	}
	g_hClientKeyValues[iClient] = CreateKeyValues("Weapons");
}

Func1611648(iClient, const String:arg16[], const String:arg20[], arg24, arg28) // address: 1611648
{
	if (arg24 && arg24 < GetTime())
	{
		Func1695224(iClient, arg20);
		return;
	}
	if (KvJumpToKey(g_hClientKeyValues[iClient], arg16, true))
	{
		if (KvJumpToKey(g_hClientKeyValues[iClient], arg20, true))
		{
			KvSetNum(g_hClientKeyValues[iClient], "time", arg24);
			if (arg28)
			{
				Func1613552(iClient, arg16, arg20, arg24);
			}
			KvRewind(g_hClientKeyValues[iClient]);
		}
		KvRewind(g_hClientKeyValues[iClient]);
	}
}

Func1613552(iClient, const String:arg16[], const String:arg20[], arg24) // address: 1613552
{
	if (KvJumpToKey(g_var32580, g_eiClientID[iClient][szClientID], true))
	{
		if (KvJumpToKey(g_var32580, arg16, true))
		{
			new var20[5];
			GetTrieArray(g_var32848, arg20, var20, 5);
			KvSetString(g_var32580, "Name", arg20);
			KvSetNum(g_var32580, "w", var20[0]); // Array
			KvSetNum(g_var32580, "v", var20[1]); // Array
			KvSetNum(g_var32580, "v_t", var20[2]); // Array
			KvSetNum(g_var32580, "v_ct", var20[3]); // Array
			KvSetNum(g_var32580, "v_flip", var20[4]); // Array
			KvSetNum(g_var32580, "time", arg24);
			KvRewind(g_var32580);
		}
		KvRewind(g_var32580);
	}
}

Func1615784(iClient, const String:arg16[]) // address: 1615784
{
	if (KvJumpToKey(g_var32580, g_eiClientID[iClient][szClientID], false))
	{
		if (KvJumpToKey(g_var32580, arg16, false))
		{
			KvDeleteThis(g_var32580);
			KvRewind(g_var32580);
		}
		KvRewind(g_var32580);
	}
}

Func1617064(iClient, const String:arg16[], const String:arg20[]) // address: 1617064
{
	if (KvJumpToKey(g_hClientKeyValues[iClient], arg16, false))
	{
		if (KvJumpToKey(g_hClientKeyValues[iClient], arg20, false))
		{
			KvDeleteThis(g_hClientKeyValues[iClient]);
			KvRewind(g_hClientKeyValues[iClient]);
		}
		KvRewind(g_hClientKeyValues[iClient]);
	}
}

Func1618504(iClient,  const String:arg16[]) // address: 1618504
{
	if (KvJumpToKey(g_hClientKeyValues[iClient], arg16, false))
	{
		KvDeleteThis(g_hClientKeyValues[iClient]);
		KvRewind(g_hClientKeyValues[iClient]);
	}
}

ClearClient(iClient) // address: 1619800 
{
	if (KvJumpToKey(g_var32580, g_eiClientID[iClient][szClientID], false))
	{
		KvDeleteThis(g_var32580);
		KvRewind(g_var32580);
	}
	if (g_hClientKeyValues[iClient])
	{
		CloseHandle(g_hClientKeyValues[iClient]);
		g_hClientKeyValues[iClient] = INVALID_HANDLE;
	}
}

bool:Func1621172(iClient, const String:arg16[]) // address: 1621172
{
	new bool:var4 = false;
	if (KvJumpToKey(g_var32580, g_eiClientID[iClient][szClientID], false))
	{
		if (KvJumpToKey(g_var32580, arg16, false))
		{
			KvRewind(g_var32580);
			var4 = true;
		}
		KvRewind(g_var32580);
	}
	return var4;
}

bool:Func1622992(iClient, const String:arg16[], const String:arg20[]) // address: 1622992
{
	decl String:var52[52];
	if (KvJumpToKey(g_var32580, g_eiClientID[iClient][szClientID], false))
	{
		if (KvJumpToKey(g_var32580, arg16, false))
		{
			KvGetString(g_var32580, "Name", var52, 51);
			KvRewind(g_var32580);
		}
		KvRewind(g_var32580);
	}
	return (!strcmp(arg20, var52, true)) ? true : false;
}

bool:Func1624584(iClient, const String:arg16[]) // address: 1624584
{
	new bool:var4 = false;
	if (KvJumpToKey(g_hClientKeyValues[iClient], arg16, false))
	{
		var4 = true;
		KvRewind(g_hClientKeyValues[iClient]);
	}
	return var4;
}

bool:Func1626196(iClient, const String:arg16[], const String:arg20[]) // address: 1626196
{
	new bool:var4 = false;
	if (KvJumpToKey(g_hClientKeyValues[iClient], arg16, false))
	{
		if (KvJumpToKey(g_hClientKeyValues[iClient], arg20, false))
		{
			var4 = true;
			KvRewind(g_hClientKeyValues[iClient]);
		}
		KvRewind(g_hClientKeyValues[iClient]);
	}
	return var4;
}

Func1628244(iClient, const String:arg16[], const String:arg20[]) // address: 1628244
{
	new var4 = -1;
	if (KvJumpToKey(g_hClientKeyValues[iClient], arg16, false))
	{
		if (KvJumpToKey(g_hClientKeyValues[iClient], arg20, false))
		{
			var4 = KvGetNum(g_hClientKeyValues[iClient], "time", 0);
			KvRewind(g_hClientKeyValues[iClient]);
		}
		KvRewind(g_hClientKeyValues[iClient]);
	}
	return var4;
}

Func1629704(iClient, const String:arg16[], String:arg20[51]) // address: 1629704
{
	if (KvJumpToKey(g_var32580, g_eiClientID[iClient][szClientID], false))
	{
		if (KvJumpToKey(g_var32580, arg16, false))
		{
			KvGetString(g_var32580, "Name", arg20, 51);
			KvRewind(g_var32580);
		}
		KvRewind(g_var32580);
	}
}

GetClientC4Model(iClient) // address: 1631052
{
	new var4 = -1;
	if (KvJumpToKey(g_var32580, g_eiClientID[iClient][szClientID], true))
	{
		if (KvJumpToKey(g_var32580, "c4", true))
		{
			var4 = KvGetNum(g_var32580, "v_flip", -1);
			KvRewind(g_var32580);
		}
		KvRewind(g_var32580);
	}
	return var4;
}

Func1632396(iClient, const String:arg16[], const String:arg20[], arg24) // address: 1632396
{
	if (Func1622992(iClient, arg16, arg20))
	{
		Func1615784(iClient, arg16);
	}
	if (arg24)
	{
		Func1617064(iClient, arg16, arg20);
	}
	Func1695224(iClient, arg20);
}

Func1634244(Handle:iClient) // address: 1634244
{
	if (KvGotoFirstSubKey(g_hModelsKeyValues, true))
	{
		decl String:var16[16];
		decl String:var68[52];
		do
		{
			KvGetSectionName(g_hModelsKeyValues, var16, 15);
			KvGetString(g_hModelsKeyValues, "title", var68, 50, "NULL");
			AddMenuItem(iClient, var16, var68, 0);
		}
		while (KvGotoNextKey(g_hModelsKeyValues, true));
		KvRewind(g_hModelsKeyValues);
	}
}

Func1636308(iClient, const String:arg16[], Handle:arg20) // address: 1636308
{
	if (KvJumpToKey(g_hModelsKeyValues, arg16, false))
	{
		if (KvGotoFirstSubKey(g_hModelsKeyValues, true))
		{
			decl String:var52[52];
			decl String:var116[64];
			do
			{
				KvGetSectionName(g_hModelsKeyValues, var52, 51);
				if (Func1638632(iClient, arg16, var52))
				{
					FormatEx(var116, 61, "%s [+]", var52);
					AddMenuItem(arg20, var52, var116, 1);
				}
				else
				{
					AddMenuItem(arg20, var52, var52, 0);
				}
			}
			while (KvGotoNextKey(g_hModelsKeyValues, true));
			KvRewind(g_hModelsKeyValues);
		}
	}
}

bool:Func1638632(iClient, const String:arg16[], const String:arg20[]) // address: 1638632
{
	new bool:var4 = false;
	new var8 = GetClientOfUserId(g_eClientInfo[iClient][UserID]);
	if (var8 < 1)
	{
		var4 = true;
	}
	if (KvJumpToKey(g_hClientKeyValues[var8], arg16, false))
	{
		if (KvJumpToKey(g_hClientKeyValues[var8], arg20, false))
		{
			new var12 = KvGetNum(g_hClientKeyValues[var8], "time", 0);
			if (var12 && var12 < GetTime())
			{
				Func1632396(var8, arg16, arg20, 0);
				Func1599824(var8, arg16, 1);
				KvDeleteThis(g_hClientKeyValues[var8]);
			}
			else
			{
				var4 = true;
			}
			KvRewind(g_hClientKeyValues[var8]);
		}
		KvRewind(g_hClientKeyValues[var8]);
	}
	return var4;
}

Func1640440(iClient, Handle:hMenu) // address: 1640440
{
	if (KvGotoFirstSubKey(g_hClientKeyValues[iClient], true))
	{
		decl String:var16[16];
		decl String:var68[52];
		do
		{
			KvGetSectionName(g_hClientKeyValues[iClient], var16, 15);
			if (KvJumpToKey(g_hModelsKeyValues, var16, false))
			{
				KvGetString(g_hModelsKeyValues, "title", var68, 50, "NULL");
				KvRewind(g_hModelsKeyValues);
			}
			AddMenuItem(hMenu, var16, var68, 0);
		}
		while (KvGotoNextKey(g_hClientKeyValues[iClient], false));
	}
	KvRewind(g_hClientKeyValues[iClient]);
}

Func1642004(iClient, const String:szWeapon[], Handle:hMenu, bool:arg24) // address: 1642004
{
	decl String:szModelName[52];
	if (KvJumpToKey(g_hClientKeyValues[iClient], arg16, false))
	{
		if (KvGotoFirstSubKey(g_hClientKeyValues[iClient], true))
		{
			decl String:szDisplay[80];
			do
			{
				KvGetSectionName(g_hClientKeyValues[iClient], szModelName, sizeof(szModelName));
				new iExpired = KvGetNum(g_hClientKeyValues[iClient], "time", 0);
				if (iExpired && iExpired < GetTime())
				{
					Func1632396(iClient, szWeapon, szModelName, 0);
					Func1599824(iClient, szWeapon, 1);
					KvDeleteThis(g_hClientKeyValues[iClient]);
				}
				else
				{
					if (arg24 && Func1622992(iClient, szWeapon, szModelName))
					{
						FormatMenuItem(szDisplay, szModelName, iExpired, true);
						AddMenuItem(hMenu, szModelName, szDisplay, 0);
						arg24 = false;
					}
					else
					{
						FormatMenuItem(szDisplay, szModelName, iExpired, false);
						AddMenuItem(hMenu, szModelName, szDisplay, 0);
					}
				}
			}
			while (KvGotoNextKey(g_hClientKeyValues[iClient], true));
			KvRewind(g_hClientKeyValues[iClient]);
		}
	}
	KvRewind(g_hClientKeyValues[iClient]);
}

FormatMenuItem(String:szBuffer[80], const String:szModelName[], iExpired, bool:bStatus) // address: 1644592
{
	if (iExpired)
	{
		new iLeft = iExpired - GetTime();
		FormatEx(szBuffer, 80, "%s%s\n[%dдн. %dч. %dм.]\n ", 
			szModelName, bStatus ? " [+]" : "", iLeft / 3600 / 24, iLeft / 3600 % 24, iLeft / 60 % 60);
	}
	else
	{
		FormatEx(szBuffer, 70, "%s%s\n[Навсегда]\n ", szModelName, bStatus ? " [+]" : "");
	}
}

GetClientWeaponWorldModel(iClient, const String:szWeapon[]) // address: 1646368
{
	new iModel = 0;
	if (KvJumpToKey(g_var32580, g_eiClientID[iClient][szClientID], false))
	{
		if (KvJumpToKey(g_var32580, szWeapon, false))
		{
			iModel = KvGetNum(g_var32580, "w", 0);
			KvRewind(g_var32580);
		}
		KvRewind(g_var32580);
	}
	return iModel;
}

GetClientWeaponModels(iClient, iModels[4], const String:arg20[]) // address: 1648240
{
	new bool:var4 = false;
	if (g_iCvarRestrictMode > 0 && KvJumpToKey(g_hDropKeyValues, g_eiClientID[iClient][szClientID], false))
	{
		if (KvJumpToKey(g_hDropKeyValues, arg20, false))
		{
			iModels[0] = KvGetNum(g_hDropKeyValues, "w", 0); // Array
			iModels[1] = KvGetNum(g_hDropKeyValues, "v", 0); // Array
			iModels[2] = KvGetNum(g_hDropKeyValues, "v_flip", 0); // Array
			KvRewind(g_hDropKeyValues);
			var4 = true;
		}
		KvRewind(g_hDropKeyValues);
	}
	if (!var4)
	{
		if (KvJumpToKey(g_var32580, g_eiClientID[iClient][szClientID], false))
		{
			if (KvJumpToKey(g_var32580, arg20, false))
			{
				iModels[0] = KvGetNum(g_var32580, "w", 0); // Array
				iModels[1] = KvGetNum(g_var32580, "v", 0); // Array
				if (iModels[1] < 1) // Array
				{
					switch (GetClientTeam(iClient))
					{
						case 2:
						{
							iModels[1] = KvGetNum(g_var32580, "v_t", 0); // Array
						}
						case 3:
						{
							iModels[1] = KvGetNum(g_var32580, "v_ct", 0); // Array
						}
					}
				}
				iModels[2] = KvGetNum(g_var32580, "v_flip", 0); // Array
				iModels[3] = KvGetNum(g_var32580, "time", 0); // Array
				KvRewind(g_var32580);
			}
			KvRewind(g_var32580);
		}
	}
}

Func1650448(iClient, arg16, const String:arg20[]) // address: 1650448
{
	decl String:var8[8];
	decl String:var60[52];
	var8[0] = '\0'; // Array
	IntToString(arg16, var8, 5);
	if (GetTrieString(g_hTrieWeaponNames, var8, var60, 50))
	{
		Func1653216(iClient, arg20, var60);
	}
}

bool:Func1651704(iClient, const String:arg16[], String:arg20[50]) // address: 1651704
{
	new bool:var4 = false;
	if (KvJumpToKey(g_var32580, g_eiClientID[iClient][szClientID], false))
	{
		if (KvJumpToKey(g_var32580, arg16, false))
		{
			KvGetString(g_var32580, "Name", arg20, 50);
			KvRewind(g_var32580);
			var4 = true;
		}
		KvRewind(g_var32580);
	}
	return var4;
}

Func1653216(iClient, const String:arg16[], const String:arg20[]) // address: 1653216
{
	if (KvJumpToKey(g_hDropKeyValues, g_eiClientID[iClient][szClientID], true))
	{
		if (KvJumpToKey(g_hDropKeyValues, arg16, true))
		{
			new var20[5];
			GetTrieArray(g_var32848, arg20, var20, 5);
			KvSetNum(g_hDropKeyValues, "w", var20[0]); // Array
			if (var20[1] < 1) // Array
			{
				switch (GetClientTeam(iClient))
				{
					case 2:
					{
						KvSetNum(g_hDropKeyValues, "v", var20[2]); // Array
					}
					case 3:
					{
						KvSetNum(g_hDropKeyValues, "v", var20[3]); // Array
					}
				}
			}
			else
			{
				KvSetNum(g_hDropKeyValues, "v", var20[1]); // Array
			}
			KvSetNum(g_hDropKeyValues, "v_flip", var20[4]); // Array
			KvRewind(g_hDropKeyValues);
		}
		KvRewind(g_hDropKeyValues);
	}
}

Func1655144(iClient, const String:arg16[]) // address: 1655144
{
	if (KvJumpToKey(g_hDropKeyValues, g_eiClientID[iClient][szClientID], false))
	{
		if (KvJumpToKey(g_hDropKeyValues, arg16, false))
		{
			KvDeleteThis(g_hDropKeyValues);
			KvRewind(g_hDropKeyValues);
		}
		KvRewind(g_hDropKeyValues);
	}
}

public Action:CS_OnTerminateRound(&Float:delay, &CSRoundEndReason:reason) // address: 1656928
{
	if (g_iCvarRestrictMode == 1)
	{
		CreateTimer(delay - 0.7, Timer_DeleteDropModel, _, 2);
	}
}

public OnConVarRestart(Handle:convar, const String:oldValue[], const String:newValue[]) // address: 1658444
{
	if (g_iCvarRestrictMode == 1)
	{
		CreateTimer(0.5, Timer_DeleteDropModel, 0, 2);
	}
}

public Action:Timer_DeleteDropModel(Handle:timer) // address: 1659796
{
	if (g_hDropKeyValues)
	{
		CloseHandle(g_hDropKeyValues);
		g_hDropKeyValues = CreateKeyValues("Drop");
	}
}

ClearClientDrop(iClient) // address: 1661392
{
	if (KvJumpToKey(g_hDropKeyValues, g_eiClientID[iClient][szClientID], false))
	{
		KvDeleteThis(g_hDropKeyValues);
		KvRewind(g_hDropKeyValues);
	}
}

ReadDownloadlist(const String:szPath[]) // address: 1662864
{
	new Handle:hFile = OpenFile(szPath, "r");
	if (hFile == INVALID_HANDLE)
	{
		return false;
	}
	decl String:szBuffer[256], index;
	while (!IsEndOfFile(hFile) && ReadFileLine(hFile, szBuffer, sizeof(szBuffer)))
	{
		index = StrContains(szBuffer, "//", true);
		if (index != -1)
		{
			szBuffer[index] = '\0';
		}
		index = StrContains(szBuffer, "#", true);
		if (index != -1)
		{
			szBuffer[index] = '\0';
		}
		index = StrContains(szBuffer, ";", true);
		if (index != -1)
		{
			szBuffer[index] = '\0';
		}
		TrimString(szBuffer);
		if (szBuffer[0] == '\0') // Array
		{
			continue;
		}
		Func1665392(szBuffer, 1);
	}
	CloseHandle(hFile);
	return true;
}

Func1665392(String:iClient[], arg16) // address: 1665392
{
	if (iClient[0] == '\0') // Array
	{
		return;
	}
	new var4 = strlen(iClient) + -1;
	if (iClient[var4] == '\\' || iClient[var4] == '/')
	{
		iClient[var4] = '\0';
	}
	if (FileExists(iClient, false))
	{
		decl String:var8[4];
		Func1669036(iClient, var8, 4);
		if (StrEqual(var8, "bz2", false) || StrEqual(var8, "ztmp", false))
		{
			return;
		}
		if (StrEqual(var8, "txt", false) || StrEqual(var8, "ini", false))
		{
			ReadDownloadlist(iClient);
			return;
		}
		AddFileToDownloadsTable(iClient);
	}
	else
	{
		if (arg16 && DirExists(iClient))
		{
			decl String:var260[256];
			new Handle:var264 = OpenDirectory(iClient);
			while (ReadDirEntry(var264, var260, 256))
			{
				if (StrEqual(var260, ".", true) || StrEqual(var260, "..", true))
				{
					continue;
				}
				Format(var260, 256, "%s/%s", iClient, var260);
				Func1665392(var260, arg16);
			}
			CloseHandle(var264);
		}
		else
		{
			if (FindCharInString(iClient, '*', true))
			{
				decl String:var8[4];
				Func1669036(iClient, var8, 4);
				if (StrEqual(var8, "*", true))
				{
					decl String:var264[256];
					decl String:var520[256];
					decl String:var776[256];
					Func1670368(iClient, var264, 256);
					Func1671924(iClient, var520, 256);
					StrCat(var520, 256, ".");
					new Handle:var780 = OpenDirectory(var264);
					while (ReadDirEntry(var780, var776, 256))
					{
						if (StrEqual(var776, ".", true) || StrEqual(var776, "..", true))
						{
							continue;
						}
						if (strncmp(var776, var520, strlen(var520), true) == 0)
						{
							Format(var776, 256, "%s/%s", var264, var776);
							Func1665392(var776, arg16);
						}
					}
					CloseHandle(var780);
				}
			}
		}
	}
}

Func1669036(const String:iClient[], String:arg16[], arg20) // address: 1669036
{
	new var4 = FindCharInString(iClient, '.', true);
	if (var4 == -1)
	{
		arg16[0] = '\0'; // Array
		return;
	}
	strcopy(arg16, arg20, iClient[++var4]);
}

Func1670368(const String:iClient[], String:arg16[], arg20) // address: 1670368
{
	if (iClient[0] == '\0') // Array
	{
		arg16[0] = 0; // Array
		return;
	}
	new var4 = FindCharInString(iClient, '/', true);
	if (var4 == -1)
	{
		var4 = FindCharInString(iClient, '\\', true);
		if (var4 == -1)
		{
			arg16[0] = 0; // Array
			return;
		}
	}
	strcopy(arg16, arg20, iClient);
	arg16[var4] = '\0';
}

Func1671924(const String:iClient[], String:arg16[], arg20) // address: 1671924
{
	if (iClient[0] == '\0') // Array
	{
		arg16[0] = '\0'; // Array
		return;
	}
	Func1673684(iClient, arg16, arg20);
	new var4 = FindCharInString(arg16, '.', true);
	if (var4 != -1)
	{
		arg16[var4] = '\0';
	}
}

Func1673684(const String:iClient[], String:arg16[], arg20) // address: 1673684
{
	if (iClient[0] == '\0') // Array
	{
		arg16[0] = '\0'; // Array
		return;
	}
	new var4 = FindCharInString(iClient, '/', true);
	if (var4 == -1)
	{
		var4 = FindCharInString(iClient, '\\', true);
	}
	var4++;
	strcopy(arg16, arg20, iClient[var4]);
}

bool:Func1675440(const String:iClient[], const String:arg16[]) // address: 1675440
{
	if (KvJumpToKey(g_hModelsKeyValues, arg16, false))
	{
		if (KvJumpToKey(g_hModelsKeyValues, iClient, false))
		{
			KvRewind(g_hModelsKeyValues);
			return false;
		}
		KvRewind(g_hModelsKeyValues);
	}
	return true;
}

PrepareWeaponName(const String:iClient[], String:arg16[], arg20) // address: 1677348
{
	arg20--;
	new var4 = 0;
	while (iClient[var4] || var4 < arg20)
	{
		if (IsCharUpper(iClient[var4]))
		{
			arg16[var4] = CharToLower(iClient[var4]);
		}
		else
		{
			arg16[var4] = iClient[var4];
		}
		var4++;
	}
	arg16[var4] = 0;
}
