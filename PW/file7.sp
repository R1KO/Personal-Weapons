// Decompilation by Maxim "Kailo" Telezhenko, 2018

public Native_GetIDClient(Handle:plugin, numParams) // address: 1720036
{
	new iClient = GetNativeCell(1);
	if (iClient < 1 || iClient > MaxClients)
	{
		return ThrowNativeError(1, "Invalid client index (%d)", iClient);
	}
	if (!IsClientConnected(iClient))
	{
		return ThrowNativeError(1, "Client %d is not connected", iClient);
	}
	return g_eiClientID[iClient][iClientID]; // Array
}

public Native_SetWeapon(Handle:plugin, numParams) // address: 1721412
{
	new iClient = GetNativeCell(1);
	if (iClient < 1 || iClient > MaxClients)
	{
		ThrowNativeError(1, "Invalid client index (%d)", iClient);
	}
	if (!IsClientConnected(iClient))
	{
		ThrowNativeError(1, "Client %d is not connected", iClient);
	}
	decl String:var36[32];
	GetNativeString(2, var36, 32);
	new var40 = GetStartWeaponName(var36);
	new var44 = 0;
	if (KvJumpToKey(g_hModelsKeyValues, var36[var40], false))
	{
		decl String:var96[52];
		GetNativeString(3, var96, 50);
		if (KvJumpToKey(g_hModelsKeyValues, var96, false))
		{
			KvRewind(g_hModelsKeyValues);
			if (!Func1626196(iClient, var36[var40], var96))
			{
				Func1693328(iClient, var36[var40], var96, GetNativeCell(4), true);
				Func1599824(iClient, var36[var40], 0);
				var44 = 1;
			}
		}
		else
		{
			ThrowNativeError(3, "Skin - %s not found", var96);
		}
		KvRewind(g_hModelsKeyValues);
	}
	else
	{
		ThrowNativeError(2, "Weapon - %s not found", var36);
	}
	return var44;
}

public Native_DelWeapon(Handle:plugin, numParams) // address: 1723712
{
	new iClient = GetNativeCell(1);
	if (iClient < 1 || iClient > MaxClients)
	{
		ThrowNativeError(1, "Invalid client index (%d)", iClient);
	}
	if (!IsClientConnected(iClient))
	{
		ThrowNativeError(1, "Client %d is not connected", iClient);
	}
	decl String:var36[32];
	GetNativeString(2, var36, 32);
	new var40 = GetStartWeaponName(var36);
	new var44 = 0;
	if (KvJumpToKey(g_hModelsKeyValues, var36[var40], false))
	{
		decl String:var96[52];
		GetNativeString(3, var96, 50);
		if (KvJumpToKey(g_hModelsKeyValues, var96, false))
		{
			if (Func1626196(iClient, var36[var40], var96))
			{
				Func1632396(iClient, var36[var40], var96, 1);
				Func1599824(iClient, var36[var40], 1);
				var44 = 1;
			}
			KvRewind(g_hModelsKeyValues);
		}
		else
		{
			ThrowNativeError(3, "Skin %s not found", var96);
		}
		KvRewind(g_hModelsKeyValues);
	}
	else
	{
		ThrowNativeError(2, "Weapon %s not found", var36);
	}
	return var44;
}

public Native_CheckSkinWeapon(Handle:plugin, numParams) // address: 1726472
{
	new iClient = GetNativeCell(1);
	if (iClient < 1 || iClient > MaxClients)
	{
		return ThrowNativeError(1, "Invalid client index (%d)", iClient);
	}
	if (!IsClientConnected(iClient))
	{
		return ThrowNativeError(1, "Client %d is not connected", iClient);
	}
	decl String:var36[32];
	GetNativeString(2, var36, 32);
	new var40 = GetStartWeaponName(var36);
	new var44 = 0;
	if (KvJumpToKey(g_hModelsKeyValues, var36[var40], false))
	{
		decl String:var96[52];
		GetNativeString(3, var96, 50);
		if (KvJumpToKey(g_hModelsKeyValues, var96, false))
		{
			KvRewind(g_hModelsKeyValues);
			if (GetNativeCell(4))
			{
				var44 = Func1622992(iClient, var36[var40], var96);
			}
			else
			{
				var44 = Func1626196(iClient, var36[var40], var96);
			}
		}
		else
		{
			ThrowNativeError(3, "Skin %s not found", var96);
		}
		KvRewind(g_hModelsKeyValues);
	}
	else
	{
		ThrowNativeError(2, "Weapon %s not found", var36);
	}
	return var44;
}

public Native_CheckWeapon(Handle:plugin, numParams) // address: 1728700
{
	new iClient = GetNativeCell(1);
	if (iClient < 1 || iClient > MaxClients)
	{
		ThrowNativeError(1, "Invalid client index (%d)", iClient);
	}
	if (!IsClientConnected(iClient))
	{
		ThrowNativeError(1, "Client %d is not connected", iClient);
	}
	decl String:var36[32];
	GetNativeString(2, var36, 32);
	new var40 = GetStartWeaponName(var36);
	new var44 = 0;
	if (KvJumpToKey(g_hModelsKeyValues, var36[var40], false))
	{
		new var48 = GetNativeCell(3);
		if (var48)
		{
			var44 = Func1621172(iClient, var36[var40]);
		}
		else
		{
			var44 = Func1624584(iClient, var36[var40]);
		}
		KvRewind(g_hModelsKeyValues);
	}
	else
	{
		ThrowNativeError(2, "Weapon %s not found", var36);
	}
	return var44;
}

public Native_EnableWeapon(Handle:plugin, numParams) // address: 1731360
{
	new iClient = GetNativeCell(1);
	if (iClient < 1 || iClient > MaxClients)
	{
		ThrowNativeError(1, "Invalid client index (%d)", iClient);
	}
	if (!IsClientConnected(iClient))
	{
		ThrowNativeError(1, "Client %d is not connected", iClient);
	}
	new var8 = GetNativeCell(2);
	switch (var8)
	{
		case 0, 1:
		{
			return Func1732860(iClient, var8);
		}
		case 2:
		{
			return g_var7768[iClient];
		}
	}
	return 0;
}

Func1732860(iClient, arg16) // address: 1732860
{
	if (g_var7768[iClient] != arg16)
	{
		g_var7768[iClient] = arg16;
		new iWeapon = GetActiveWeapon(iClient);
		if (iClient > 0)
		{
			Func1541204(iClient, iWeapon, GetSequence(m_hViewModelPred[iClient][0]));
			g_var10408[iClient] = 0;
		}
		decl String:szQuery[256];
		FormatEx(szQuery, sizeof(szQuery), "UPDATE `user` SET `status` = %d WHERE `custom_id` = %d", arg16, g_eiClientID[iClient][iClientID]); // Array
		SQL_TQuery(g_hDatabase, SQL_DefCallback, szQuery);
	}
	return 1;
}

public Native_CheckNewClient(Handle:plugin, numParams) // address: 1734772
{
	new iClient = GetNativeCell(1);
	if (iClient < 1 || iClient > MaxClients)
	{
		return ThrowNativeError(1, "Invalid client index (%d)", iClient);
	}
	if (!IsClientConnected(iClient))
	{
		return ThrowNativeError(1, "Client %d is not connected", iClient);
	}
	return g_iIsNewClient[iClient];
}

public Native_SequenceClient(Handle:plugin, numParams) // address: 1736028
{
	new iClient = GetNativeCell(1);
	if (iClient < 1 || iClient > MaxClients)
	{
		ThrowNativeError(1, "Invalid client index (%d)", iClient);
	}
	if (!IsClientConnected(iClient))
	{
		ThrowNativeError(1, "Client %d is not connected", iClient);
	}
	g_var9352[iClient] = GetNativeCell(2);
}

public Native_AddTimeWeapon(Handle:plugin, numParams) // address: 1738004
{
	new iClient = GetNativeCell(1);
	if (iClient < 1 || iClient > MaxClients)
	{
		return ThrowNativeError(1, "Invalid client index (%d)", iClient);
	}
	if (!IsClientConnected(iClient))
	{
		return ThrowNativeError(1, "Client %d is not connected", iClient);
	}
	decl String:var36[32];
	GetNativeString(2, var36, 32);
	new var40 = GetStartWeaponName(var36);
	new var44 = 0;
	if (KvJumpToKey(g_hModelsKeyValues, var36[var40], false))
	{
		decl String:var96[52];
		GetNativeString(3, var96, 50);
		if (KvJumpToKey(g_hModelsKeyValues, var96, false))
		{
			if (Func1626196(iClient, var36[var40], var96))
			{
				Func1693328(iClient, var36[var40], var96, GetNativeCell(4), false);
				var44 = 1;
			}
			KvRewind(g_hModelsKeyValues);
		}
		else
		{
			ThrowNativeError(3, "Skin %s not found", var96);
		}
		KvRewind(g_hModelsKeyValues);
	}
	else
	{
		ThrowNativeError(2, "Weapon %s not found", var36);
	}
	return var44;
}
