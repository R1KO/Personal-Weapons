// Decompilation by Maxim "Kailo" Telezhenko, 2018

new Handle:g_hStatsKeyValues = INVALID_HANDLE; // address: 2096 // codestart: 0
new g_var2100[MAXPLAYERS+1][3]; // address: 2100 // zero // codestart: 0
new g_var3156[MAXPLAYERS+1]; // address: 3156 // zero // codestart: 0
new g_var3420[MAXPLAYERS+1]; // address: 3420 // zero // codestart: 0
new String:g_var3684[MAXPLAYERS+1][16]; // address: 3684 // codestart: 0
new g_var5004[MAXPLAYERS+1]; // address: 5004 // zero // codestart: 0
new String:g_var5268[MAXPLAYERS+1][32]; // address: 5268 // zero // codestart: 0
new bool:g_iCvarKIllModels = false; // address: 7644 // codestart: 0
new bool:g_iCvarKIllAWPModels = false; // address: 7648 // codestart: 0
new bool:g_iCvarKillKnife = false; // address: 7652 // codestart: 0
new bool:g_iCvarWebMotd = false; // address: 7656 // codestart: 0
// 7660 global address

Func1484996() // address: 1484996
{
	g_hStatsKeyValues = CreateKeyValues("stats");

	HookEvent("player_death", player_death, EventHookMode_Post);
	
	new Handle:hCvar = CreateConVar("sm_weapons_kill", "1", "Выдавать бонусное оружие по килам", 0, true, 0.0, true, 1.0);
	g_iCvarKIllModels = GetConVarBool(hCvar);
	HookConVarChange(hCvar, Cvar_WeaponsKill);

	hCvar = CreateConVar("sm_weapons_kill_awp", "1", "Выдавать бонусное оружие по килам с авп", 0, true, 0.0, true, 1.0);
	g_iCvarKIllAWPModels = GetConVarBool(hCvar);
	HookConVarChange(hCvar, Cvar_WeaponsKillAwp);

	hCvar = CreateConVar("sm_weapons_kill_knife", "1", "Выдавать бонусное оружие по килам с ножа", 0, true, 0.0, true, 1.0);
	g_iCvarKillKnife = GetConVarBool(hCvar);
	HookConVarChange(hCvar, Cvar_WeaponsKillKnife);

	hCvar = CreateConVar("sm_webmotd", "1", "Включить показ мотд окна игроку выигравшему бонус", 0, true, 0.0, true, 1.0);
	g_iCvarWebMotd = GetConVarBool(hCvar);
	HookConVarChange(hCvar, Cvar_WeaponsMotd);

	hCvar = CreateConVar("sm_weapons_restrict", "0", "Если игрок поднял оружие со скином. 0 - отключить. 1 - удалить скин в след раунде. 2 - удалить скин если умер.", 0, true, 0.0, true, 2.0);
	g_iCvarRestrictMode = GetConVarInt(hCvar);
	HookConVarChange(hCvar, OnConVarChangeRestrict);
}

public OnConVarChangeRestrict(Handle:convar, const String:oldValue[], const String:newValue[]) // address: 1500888
{
	g_iCvarRestrictMode = StringToInt(newValue, 10);
}

public Cvar_WeaponsKill(Handle:convar, const String:oldValue[], const String:newValue[]) // address: 1487024
{
	g_iCvarKIllModels = bool:StringToInt(newValue, 10);
}

public Cvar_WeaponsKillAwp(Handle:convar, const String:oldValue[], const String:newValue[]) // address: 1488508
{
	g_iCvarKIllAWPModels = bool:StringToInt(newValue, 10);
}

public Cvar_WeaponsKillKnife(Handle:convar, const String:oldValue[], const String:newValue[]) // address: 1490416
{
	g_iCvarKillKnife = bool:StringToInt(newValue, 10);
}

public Cvar_WeaponsMotd(Handle:convar, const String:oldValue[], const String:newValue[]) // address: 1492120
{
	g_iCvarWebMotd = bool:StringToInt(newValue, 10);
}

Func1493428() // address: 1493428
{
	KvRewind(g_hStatsKeyValues);
	if (KvGotoFirstSubKey(g_hStatsKeyValues, true))
	{
		do
		{
			KvDeleteThis(g_hStatsKeyValues);
		}
		while (KvGotoNextKey(g_hStatsKeyValues, true));
	}
}

Func1494740() // address: 1494740
{
	KvRewind(g_hStatsKeyValues);
	if (KvGotoFirstSubKey(g_hStatsKeyValues, true))
	{
		do {
			for (new i = 0; i < 3; i++)
			{
				KvSetNum(g_hStatsKeyValues, g_sWepons[i], 0);
			}
			KvSetNum(g_hStatsKeyValues, "kill", 0);
			KvSetNum(g_hStatsKeyValues, "death", 0);
		} while (KvGotoNextKey(g_hStatsKeyValues, true));
	}
	for (new i = 1; i <= MaxClients; ++i)
	{
		if (IsClientInGame(vair4))
		{
			g_var2100[i][0] = 0; // Array
			g_var2100[i][1] = 0; // Array
			g_var2100[i][2] = 0; // Array
			g_var3420[i] = 0;
			g_var3156[i] = 0;
		}
	}
}

Func1496940(iClient) // address: 1496940
{
	if (IsFakeClient(iClient))
	{
		return;
	}
	GetClientAuthString(iClient, g_var5268[iClient], 30, true);
	KvRewind(g_hStatsKeyValues);
	if (KvJumpToKey(g_hStatsKeyValues, g_var5268[iClient], false))
	{
		for (new var4 = 0; var4 < 3; var4++)
		{
			g_var2100[iClient][var4] = KvGetNum(g_hStatsKeyValues, g_sWepons[var4], 0);
		}
		g_var3156[iClient] = KvGetNum(g_hStatsKeyValues, "kill", 0);
		g_var3420[iClient] = KvGetNum(g_hStatsKeyValues, "death", 0);
		new Handle:var4 = Handle:KvGetNum(g_hStatsKeyValues, "time", 0);
		if (var4)
		{
			KillTimer(var4, false);
		}
	}
	else
	{
		if (KvJumpToKey(g_hStatsKeyValues, g_var5268[iClient], true))
		{
			for (new var4 = 0; var4 < 3; var4++)
			{
				KvSetNum(g_hStatsKeyValues, g_sWepons[var4], 0);
				g_var2100[iClient][var4] = 0;
			}
			g_var3420[iClient] = 0;
			g_var3156[iClient] = 0;
			KvSetNum(g_hStatsKeyValues, "kill", 0);
			KvSetNum(g_hStatsKeyValues, "death", 0);
		}
	}
}

Func1499968(iClient) // address: 1499968
{
	KvRewind(g_hStatsKeyValues);
	if (KvJumpToKey(g_hStatsKeyValues, g_var5268[iClient], false))
	{
		for (new i = 0; i < 3; i++)
		{
			KvSetNum(g_hStatsKeyValues, g_sWepons[i], g_var2100[iClient][i]);
			g_var2100[iClient][i] = 0;
		}
		KvSetNum(g_hStatsKeyValues, "kill", g_var3156[iClient]);
		KvSetNum(g_hStatsKeyValues, "death", g_var3420[iClient]);
		g_var3420[iClient] = 0;
		g_var3156[iClient] = 0;
		new Handle:hTimer = INVALID_HANDLE;
		new Handle:hDataPack = INVALID_HANDLE;
		hTimer = CreateDataTimer(float(300), DeleteStatsTime, hDataPack, 2);
		WritePackString(hDataPack, g_var5268[iClient]);
		KvSetNum(g_hStatsKeyValues, "time", _:hTimer);
	}
}

public Action:DeleteStatsTime(Handle:timer, any:data) // address: 1502248
{
	decl String:var28[28];
	ResetPack(data, false);
	ReadPackString(data, var28, 25);
	KvRewind(g_hStatsKeyValues);
	if (KvJumpToKey(g_hStatsKeyValues, var28, false))
	{
		KvDeleteThis(g_hStatsKeyValues);
	}
}


public player_death(Handle:event, const String:name[], bool:dontBroadcast) // address: 1503272
{
	new iAttaker = GetClientOfUserId(GetEventInt(event, "attacker"));
	if (iAttaker < 1)
	{
		return;
	}
	new iClient = GetClientOfUserId(GetEventInt(event, "userid"));
	if (GetClientTeam(iAttaker) == GetClientTeam(iClient))
	{
		return;
	}
	decl String:szWeapon[32];
	GetEventString(event, "weapon", szWeapon, sizeof(szWeapon));
	for (new i = 0; i < 3; i++)
	{
		if (i == 0 && !StrEqual(szWeapon, "awp", true) && !StrEqual(szWeapon, "knife", true))
		{
			g_var2100[iAttaker][i]++;
		}
		else
		{
			if (StrEqual(szWeapon, g_sWepons[i], true))
			{
				g_var2100[iAttaker][i]++;
			}
		}
		if (g_var2100[iAttaker][i] >= g_iNeedKIlls[i])
		{
			g_var2100[iAttaker][i] = 0;
			if (GetKillType(i) && Func1505608(iAttaker, szWeapon, i, false))
			{
				break;
			}
		}
	}
	g_var3156[iAttaker]++;
	g_var3420[iClient]++;
	PlayerDeath_NextMap(iAttaker);
}

Func1505608(arg12, String:arg16[15], arg20, bool:arg24) // address: 1505608
{
	decl String:var52[51];
	var52[0] = '\0'; // Array
	GetNeedKills(g_sWepons[arg20], arg16, var52);
	if (var52[0]) // Array
	{
		g_var5004[arg12] = arg20;
		strcopy(g_var3684[arg12], 15, arg16);
		if (!arg24)
		{
			decl String:var312[260];
			if (GetTrieString(g_hSoundTrie, g_sWepons[arg20], var312, 257))
			{
				EmitSoundToAll(var312, -2, 6, 75, 0, 1.0, 100, -1, NULL_VECTOR, NULL_VECTOR, true, 0.0);
			}
		}
		switch (arg20)
		{
			case 0:
			{
				if (!arg24)
				{
					PrintCenterTextAll("ИГРОК %N ВЫИГРАЛ ОРУЖИЕ!", arg12);
				}
			}
			case 1:
			{
				PrintCenterTextAll("ИГРОК %N САМЫЙ КРУТОЙ АВАПЕР НА ЭТОЙ КАРТЕ!", arg12);
			}
			case 2:
			{
				PrintCenterTextAll("ИГРОК %N САМЫЙ КРУТОЙ МЯСНИК НА ЭТОЙ КАРТЕ!", arg12);
			}
		}
		new Handle:var56 = CreateMenu(Select_Menu, MENU_ACTIONS_DEFAULT);
		decl String:var96[40];
		var96[0] = '\0'; // Array
		if (!CheckSkinWeapon_SkinsWeapons(arg12, arg16, var52, false))
		{
			Func1511924(g_iTimeBonus[arg20], var96, 40);
			PrintToChatAll("[P.W] Игрок %N выиграл оружие %s %s", arg12, var52, var96);
			SetMenuTitle(var56, "Вы выиграли оружие %s:\n \n", var52);
		}
		else
		{
			Func1511924(g_iTimeBonus[3], var96, 40); // Array
			PrintToChatAll("[P.W] Игрок %N выиграл продление срока для оружия %s %s", arg12, var52, var96);
			SetMenuTitle(var56, "Вы выиграли продление %s:\n \n", var52);
		}
		AddMenuItem(var56, var52, "Забрать", 0);
		AddMenuItem(var56, var52, "Отказ", 0);
		DisplayMenu(var56, arg12, 0);
	}
	return 0;
}

public Select_Menu(Handle:menu, MenuAction:action, param1, param2) // address: 1507844
{
	switch (action)
	{
		case 16:
		{
			CloseHandle(menu);
		}
		case 4:
		{
			if (!param2)
			{
				decl String:var52[52];
				GetMenuItem(menu, param2, var52, 51);
				if (AddWeapon_SkinsWeapons(param1, g_var3684[param1], var52, g_iTimeBonus[g_var5004[param1]]))
				{
					Func1513788(param1, var52);
					PrintToChat(param1, "[P.W] У вас новое оружие в инвентаре.");
				}
				else
				{
					if (AddTime_SkinsWeapons(param1, g_var3684[param1], var52, g_iTimeBonus[3])) // Array
					{
						PrintToChat(param1, "[P.W] Вам продлен срок на оружие.");
					}
					else
					{
						LogError("[Bonus] Failed to give the player a bonus weapons");
					}
				}
			}
			else
			{
				PrintToChat(param1, "[P.W] Вы отказались от оружия.");
			}
		}
	}
}

GetKillType(index) // address: 1510272
{
	switch (index)
	{
		case 0:
		{
			return g_iCvarKIllModels;
		}
		case 1:
		{
			return g_iCvarKIllAWPModels;
		}
		case 2:
		{
			return g_iCvarKillKnife;
		}
	}
	return 0;
}

Func1511924(arg12, String:arg16[], arg20) // address: 1511924
{
	if (arg12)
	{
		if (arg12 >= 86400)
		{
			FormatEx(arg16, arg20, "на %d дн. %d ч.", arg12 / 3600 / 24, arg12 / 3600 % 24);
		}
		else
		{
			FormatEx(arg16, arg20, "на %d ч.", arg12 / 3600 % 24);
		}
	}
	else
	{
		FormatEx(arg16, arg20, "навсегда");
	}
}

Func1513788(arg12, const String:arg16[]) // address: 1513788
{
	if (g_iCvarWebMotd)
	{
		decl String:var512[512];
		if (GetTrieString(g_hMotdTrie, arg16, var512, 512))
		{
			ShowMOTDPanel(arg12, arg16, var512, 2);
		}
	}
}

public Float:CalculatorKpd(arg12) // address: 1515140
{
	new var4 = g_var3156[arg12];
	new var8 = g_var3420[arg12];
	if (g_var3420[arg12] == 0)
	{
		var8 = 1;
	}
	return float(var4 * 100 / var8) / 100.0;
}
