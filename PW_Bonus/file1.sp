// Decompilation by Maxim "Kailo" Telezhenko, 2018

new Handle:g_hNeedKillsArray[4]; // address: 1404 // zero // codestart: 0
new Handle:g_hMotdTrie = INVALID_HANDLE; // address: 1420 // codestart: 0
new Handle:g_hSoundTrie = INVALID_HANDLE; // address: 1424 // codestart: 0
new String:g_sWepons[3][8] = {"all", "awp", "knife"}; // address: 1428 // codestart: 0
new g_iNeedKIlls[3]; // address: 1464 // zero // codestart: 0
new g_iTimeBonus[4]; // address: 1476 // zero // codestart: 0
new g_var1492[3]; // address: 1492 // zero // codestart: 0

LoadConfig() // address: 1472636
{
	g_hMotdTrie = CreateTrie();
	g_hSoundTrie = CreateTrie();
	for (new index = 0; index < 4; index++)
	{
		g_hNeedKillsArray[index] = CreateArray(15, 0);
	}
	new Handle:hBonusKv = CreateKeyValues("Bonus");
	if (!FileToKeyValues(hBonusKv, "addons/sourcemod/data/skins_weapons/bonus_weapons.txt"))
	{
		SetFailState("File not found data/skins_weapons/bonus_weapons.txt");
		return;
	}
	if (KvJumpToKey(hBonusKv, "Random", false))
	{
		if (KvGotoFirstSubKey(hBonusKv, false))
		{
			decl String:szSection[16];
			decl String:szKills[52];
			new index = 0;
			do
			{
				if (KvGetSectionName(hBonusKv, szSection, sizeof(szSection)) && szSection[0]) // Array
				{
					KvGetString(hBonusKv, NULL_STRING, szKills, sizeof(szKills));
					if (!strcmp(szSection, "kills", true))
					{
						index = 0;
						g_iNeedKIlls[index] = StringToInt(szKills);
						continue;
					}
					else
					{
						if (!strcmp(szSection, "kills_awp", true))
						{
							index = 1;
							g_iNeedKIlls[index] = StringToInt(szKills);
							continue;
						}
						else
						{
							if (!strcmp(szSection, "kills_knife", true))
							{
								index = 2;
								g_iNeedKIlls[index] = StringToInt(szKills);
								continue;
							}
						}
					}
					new j = FindStringInArray(g_hNeedKillsArray[index], szKills);
					if (j == -1)
					{
						if (index == 0)
						{
							PushArrayString(g_hNeedKillsArray[3], szSection); // Array
						}
						PushArrayString(g_hNeedKillsArray[index], szKills);
					}
					else
					{
						SetArrayString(g_hNeedKillsArray[index], j, szKills);
					}
				}
			}
			while (KvGotoNextKey(hBonusKv, false));
			KvGoBack(hBonusKv);
		}
		KvGoBack(hBonusKv);
	}
	if (KvJumpToKey(hBonusKv, "Time", false))
	{
		for (new i = 0; i < 3; i++)
		{
			g_iTimeBonus[i] = KvGetNum(hBonusKv, g_sWepons[i], 1) * 3600;
		}
		g_iTimeBonus[3] = KvGetNum(hBonusKv, "none", 1) * 3600; // Array
		KvRewind(hBonusKv);
	}
	if (KvJumpToKey(hBonusKv, "Motd", false))
	{
		if (KvGotoFirstSubKey(hBonusKv, false))
		{
			decl String:szValue[512];
			decl String:szSection[52];
			do
			{
				if (KvGetSectionName(hBonusKv, szSection, sizeof(szSection)) && szSection[0]) // Array
				{
					KvGetString(hBonusKv, NULL_STRING, szValue, sizeof(szValue));
					SetTrieString(g_hMotdTrie, szSection, szValue, true);
				}
			}
			while (KvGotoNextKey(hBonusKv, false));
			KvGoBack(hBonusKv);
		}
		KvRewind(hBonusKv);
	}
	if (KvJumpToKey(hBonusKv, "Sound", false))
	{
		decl String:szValue[260];
		decl String:szKey[12];
		for (new i = 0; i <= 3; i++)
		{
			szValue[0] = '\0'; // Array
			FormatEx(szKey, sizeof(szKey), "%s", i < 3 ? g_sWepons[i] : "nextmap");
			KvGetString(hBonusKv, szKey, szValue, sizeof(szValue));
			if (szValue[0]) // Array
			{
				SetTrieString(g_hSoundTrie, szKey, szValue, true);
			}
		}
	}
	CloseHandle(hBonusKv);
}

Func1476732(arg12, arg16) // address: 1476732
{
	for (new index = arg12; index < arg16; index++)
	{
		g_var1492[index] = 0;
		new iSize = GetArraySize(g_hNeedKillsArray[index]);
		new var12[iSize];
		for (new j = 0; j < iSize; j++)
		{
			var12[j] = j;
		}
		SortIntegers(var12, iSize, SortOrder:2);
		SetTrieArray(g_hMotdTrie, g_sWepons[index], var12, iSize, true);
	}
}

GetNeedKills(const String:arg12[], String:arg16[15], String:arg20[51]) // address: 1478144
{
	new index = -1;
	for (new i = 0; i < 3; i++)
	{
		if (!strcmp(arg12, g_sWepons[i], true))
		{
			index = i;
			break;
		}
	}
	if (index == -1)
	{
		return;
	}
	new iSize = GetArraySize(g_hNeedKillsArray[index]);
	if (iSize < 1)
	{
		return;
	}
	if (g_var1492[index] >= iSize)
	{
		Func1476732(index, index + 1);
	}
	new var12[iSize];
	strcopy(arg16, 15, g_sWepons[index]);
	if (GetTrieArray(g_hMotdTrie, arg16, var12, viSizear8))
	{
		new var16 = var12[g_var1492[index]];
		GetArrayString(g_hNeedKillsArray[index], var16, arg20, 51);
		g_var1492[index]++;
		if (!index)
		{
			GetArrayString(g_hNeedKillsArray[3], var16, arg16, 15); // Array
		}
	}
}
